package BibliotecaApp;


import BibliotecaApp.control.BibliotecaCtrl;
import BibliotecaApp.model.Carte;
import BibliotecaApp.repo.CartiRepo;
import org.junit.jupiter.api.*;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CartiRepoTest {

	private BibliotecaCtrl controller;
	private CartiRepo repository;
	private int defaultSize;
	
	@BeforeEach
	public void setUp() throws Exception {
		repository = new CartiRepo();
		controller = new BibliotecaCtrl(repository);
		defaultSize=controller.getCarti().size();
		//System.out.println(defaultSize+" "+controller.getCarti().size());
	}

	@AfterEach
	public void tearDown() throws Exception {
	}

	@Test
	public void testRepositorySize() {
		try {
			assertEquals(defaultSize, controller.getCarti().size());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAdaugaCarte1() {
		Carte c = new Carte();
		
		c.setTitlu("Book title");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("Autor");
		autori.add("Autorr");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword");
		keywords.add("keywordd");
		c.setCuvinteCheie(keywords);
		
		try {
			defaultSize=controller.getCarti().size();
			controller.adaugaCarte(c);
			assertEquals(defaultSize+1, controller.getCarti().size());
		} catch (Exception e) {
			e.printStackTrace();
		};
	}

	//testam (1) adaugarea non-valida si (2) mesajul din exceptia aruncata
	@Order(1)
	@Test
	public void testAdaugaCarte2() {
		Carte c = new Carte();
		
		c.setTitlu("");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("Autor");
		autori.add("Autorr");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword");
		keywords.add("keywordd");
		c.setCuvinteCheie(keywords);

		try {
			defaultSize=controller.getCarti().size();
			controller.adaugaCarte(c);
			assertFalse(controller.getCarti().size()== defaultSize+1);
		} catch (Exception e) {
			assertEquals("Titlu invalid!", e.getMessage());
		};
	}
	
	@Test
	public void testAdaugaCarte3() {
		Carte c = new Carte();
		
		c.setTitlu("Book title");
		
		ArrayList<String> autori = new ArrayList<>();
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword");
		keywords.add("keywordd");
		c.setCuvinteCheie(keywords);
		
		try {
			controller.adaugaCarte(c);
			assertEquals(0, 1);
		} catch (Exception e) {
			assertEquals("Lista autori vida!", e.getMessage());
		};
	}
	
	@Test
	public void testAdaugaCarte4() {
		Carte c = new Carte();
		
		c.setTitlu("Book title");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword");
		keywords.add("keywordd");
		c.setCuvinteCheie(keywords);
		
		try {
			controller.adaugaCarte(c);
			assertEquals(0, 1);
		} catch (Exception e) {
			assertEquals("Autor invalid!", e.getMessage());
		};
	}
	
	@Test
	public void testAdaugaCarte5() {
		Carte c = new Carte();
		
		c.setTitlu("Book title");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("Autor");
		autori.add("Autorr");
		c.setAutori(autori);
		
		c.setAnAparitie("aaaa");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword");
		keywords.add("keywordd");
		c.setCuvinteCheie(keywords);
		
		try {
			controller.adaugaCarte(c);
			assertEquals(0, 1);
		} catch (Exception e) {
			assertEquals("An aparitie invalid!", e.getMessage());
		};
	}
	
	@Test
	public void testAdaugaCarte6() {
		Carte c = new Carte();
		
		c.setTitlu("Book title");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("Autor");
		autori.add("Autorr");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword");
		keywords.add("keywordd");
		c.setCuvinteCheie(keywords);
		
		try {
			controller.adaugaCarte(c);
			assertEquals(0, 1);
		} catch (Exception e) {
			assertEquals("Editura invalid!", e.getMessage());
		};
	}
	
	@Test
	public void testAdaugaCarte7() {
		Carte c = new Carte();
		
		c.setTitlu("Book title");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("Autor");
		autori.add("Autorr");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		c.setCuvinteCheie(keywords);
		
		try {
			defaultSize=controller.getCarti().size();
			controller.adaugaCarte(c);
			assertEquals(defaultSize+1, controller.getCarti().size());
		} catch (Exception e) {
			e.printStackTrace();
			assertEquals(0, 1);
		};
	}
	
	@Test
	public void testAdaugaCarte8() {
		Carte c = new Carte();
		
		c.setTitlu("Book title");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("Autor");
		autori.add("Autorr");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("");
		c.setCuvinteCheie(keywords);
		
		try {
			controller.adaugaCarte(c);
			assertEquals(0, 1);
		} catch (Exception e) {
			assertEquals("Cuvant cheie invalid!", e.getMessage());
		};
	}
	
	@Test
	public void testAdaugaCarte9() {
		Carte c = new Carte();
		
		c.setTitlu("M");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("Autor");
		autori.add("Autorr");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword");
		keywords.add("keywordd");
		c.setCuvinteCheie(keywords);
		
		try {
			defaultSize=controller.getCarti().size();
			controller.adaugaCarte(c);
			assertEquals(defaultSize+1, controller.getCarti().size());
		} catch (Exception e) {
			e.printStackTrace();
			assertEquals(0, 1);
		};
	}
	
	@Test
	public void testAdaugaCarte10() {
		Carte c = new Carte();
		
		String titlu = "";
		for(int i = 0; i < 255; i++){
			titlu += "M";
		}
		c.setTitlu(titlu);
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("Autor");
		autori.add("Autorr");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword");
		keywords.add("keywordd");
		c.setCuvinteCheie(keywords);
		
		try {
			defaultSize=controller.getCarti().size();
			controller.adaugaCarte(c);
			assertEquals(defaultSize+1, controller.getCarti().size());
		} catch (Exception e) {
			e.printStackTrace();
			assertEquals(0, 1);
		};
	}
	
	@Test
	public void testAdaugaCarte11() {
		Carte c = new Carte();
		
		String titlu = "";
		for(int i = 0; i < 254; i++){
			titlu += "M";
		}
		c.setTitlu(titlu);
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("Autor");
		autori.add("Autorr");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword");
		keywords.add("keywordd");
		c.setCuvinteCheie(keywords);
		
		try {
			defaultSize=controller.getCarti().size();
			controller.adaugaCarte(c);
			assertEquals(defaultSize+1, controller.getCarti().size());
		} catch (Exception e) {
			e.printStackTrace();
			assertEquals(0, 1);
		};
	}
	
	@Test
	public void testAdaugaCarte12() {
		Carte c = new Carte();
		
		String titlu = "";
		for(int i = 0; i < 256; i++){
			titlu += "M";
		}
		c.setTitlu(titlu);
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("Autor");
		autori.add("Autorr");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword");
		keywords.add("keywordd");
		c.setCuvinteCheie(keywords);
		
		try {
			controller.adaugaCarte(c);
			assertEquals(0, 1);
		} catch (Exception e) {
			assertEquals("Titlu invalid!", e.getMessage());
		};
	}
	
	@Test
	public void testAdaugaCarte13() {
		Carte c = new Carte();
		
		c.setTitlu("M");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("D");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword");
		keywords.add("keywordd");
		c.setCuvinteCheie(keywords);
		
		try {
			defaultSize=controller.getCarti().size();
			controller.adaugaCarte(c);
			assertEquals(defaultSize+1, controller.getCarti().size());
		} catch (Exception e) {
			assertEquals(0, 1);
		};
	}
	
	@Test
	public void testAdaugaCarte14() {
		Carte c = new Carte();
		
		c.setTitlu("M");
		
		ArrayList<String> autori = new ArrayList<>();
		String autor = "";
		for(int i = 0; i < 255; i++){
			autor += "D";
		}
		autori.add(autor);
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword");
		keywords.add("keywordd");
		c.setCuvinteCheie(keywords);
		
		try {
			defaultSize=controller.getCarti().size();
			controller.adaugaCarte(c);
			assertEquals(defaultSize+1, controller.getCarti().size());
		} catch (Exception e) {
			assertEquals(0, 1);
		};
	}
	
	@Test
	public void testAdaugaCarte15() {
		Carte c = new Carte();
		
		c.setTitlu("M");
		
		ArrayList<String> autori = new ArrayList<>();
		String autor = "";
		for(int i = 0; i < 254; i++){
			autor += "D";
		}
		autori.add(autor);
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword");
		keywords.add("keywordd");
		c.setCuvinteCheie(keywords);
		
		try {
			defaultSize=controller.getCarti().size();
			controller.adaugaCarte(c);
			assertEquals(defaultSize+1, controller.getCarti().size());
		} catch (Exception e) {
			assertEquals(0, 1);
		};
	}
	
	@Test
	public void testAdaugaCarte16() {
		Carte c = new Carte();
		
		c.setTitlu("M");
		
		ArrayList<String> autori = new ArrayList<>();
		String autor = "";
		for(int i = 0; i < 256; i++){
			autor += "D";
		}
		autori.add(autor);
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword");
		keywords.add("keywordd");
		c.setCuvinteCheie(keywords);
		
		try {
			controller.adaugaCarte(c);
			assertEquals(0, 1);
		} catch (Exception e) {
			assertEquals("Autor invalid!", e.getMessage());
		};
	}
	
	@Test
	public void testAdaugaCarte17() {
		Carte c = new Carte();
		
		c.setTitlu("M");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("D");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("E");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword");
		keywords.add("keywordd");
		c.setCuvinteCheie(keywords);
		
		try {
			defaultSize=controller.getCarti().size();
			controller.adaugaCarte(c);
			assertEquals(defaultSize+1, controller.getCarti().size());
		} catch (Exception e) {
			assertEquals(0, 1);
		};
	}
	
	@Test
	public void testAdaugaCarte18() {
		Carte c = new Carte();
		
		c.setTitlu("M");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("D");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		String editura = "";
		for(int i = 0; i < 255; i++){
			editura += "E";
		}
		c.setEditura(editura);
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword");
		keywords.add("keywordd");
		c.setCuvinteCheie(keywords);
		
		try {
			defaultSize=controller.getCarti().size();
			controller.adaugaCarte(c);
			assertEquals(defaultSize+1, controller.getCarti().size());
		} catch (Exception e) {
			assertEquals(0, 1);
		};
	}
	
	@Test
	public void testAdaugaCarte19() {
		Carte c = new Carte();
		
		c.setTitlu("M");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("D");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		String editura = "";
		for(int i = 0; i < 254; i++){
			editura += "E";
		}
		c.setEditura(editura);
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword");
		keywords.add("keywordd");
		c.setCuvinteCheie(keywords);
		
		try {
			defaultSize=controller.getCarti().size();
			controller.adaugaCarte(c);
			assertEquals(defaultSize+1, controller.getCarti().size());
		} catch (Exception e) {
			assertEquals(0, 1);
		};
	}
	
	@Test
	public void testAdaugaCarte20() {
		Carte c = new Carte();
		
		c.setTitlu("M");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("D");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		String editura = "";
		for(int i = 0; i < 256; i++){
			editura += "E";
		}
		c.setEditura(editura);
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword");
		keywords.add("keywordd");
		c.setCuvinteCheie(keywords);
		
		try {
			defaultSize=controller.getCarti().size();
			controller.adaugaCarte(c);
			assertEquals(0, 1);
		} catch (Exception e) {
			assertEquals("Editura invalid!", e.getMessage());
		};
	}
	
	@Test
	public void testAdaugaCarte21() {
		Carte c = new Carte();
		
		c.setTitlu("M");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("D");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("E");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("k");
		c.setCuvinteCheie(keywords);
		
		try {
			defaultSize=controller.getCarti().size();
			controller.adaugaCarte(c);
			assertEquals(defaultSize+1, controller.getCarti().size());
		} catch (Exception e) {
			assertEquals(0, 1);
		};
	}
	
	@Test
	public void testAdaugaCarte22() {
		Carte c = new Carte();
		
		c.setTitlu("M");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("D");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("E");
		
		ArrayList<String> keywords = new ArrayList<>();
		String keyword = "";
		for(int i = 0; i < 255; i++){
			keyword += "k";
		}
		keywords.add(keyword);
		
		c.setCuvinteCheie(keywords);
		
		try {
			defaultSize=controller.getCarti().size();
			controller.adaugaCarte(c);
			assertEquals(defaultSize+1, controller.getCarti().size());
		} catch (Exception e) {
			assertEquals(0, 1);
		};
	}
	
	@Test
	public void testAdaugaCarte23() {
		Carte c = new Carte();
		
		c.setTitlu("M");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("D");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("E");
		
		ArrayList<String> keywords = new ArrayList<>();
		String keyword = "";
		for(int i = 0; i < 254; i++){
			keyword += "k";
		}
		keywords.add(keyword);
		
		c.setCuvinteCheie(keywords);
		
		try {
			defaultSize=controller.getCarti().size();
			controller.adaugaCarte(c);
			assertEquals(defaultSize+1, controller.getCarti().size());
		} catch (Exception e) {
			assertEquals(0, 1);
		};
	}
	
	@Test
	public void testAdaugaCarte24() {
		Carte c = new Carte();
		
		c.setTitlu("M");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("D");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("E");
		
		ArrayList<String> keywords = new ArrayList<>();
		String keyword = "";
		for(int i = 0; i < 256; i++){
			keyword += "k";
		}
		keywords.add(keyword);
		
		c.setCuvinteCheie(keywords);
		
		try {
			controller.adaugaCarte(c);
			assertEquals(0, 1);
		} catch (Exception e) {
			assertEquals("Cuvant cheie invalid!", e.getMessage());
		};
	}

}